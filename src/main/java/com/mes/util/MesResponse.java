package com.mes.util;

import java.util.HashMap;
import java.util.Map;

/**
 *  响应报文
 */
public class MesResponse {

    private String status = "";

    private String message = "";

    private Map<String,Object> data = new HashMap<>();


    public MesResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }
    public MesResponse(){}

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getData() {
        return data;
    }

    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "MesResponse{" +
                "status='" + status + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
