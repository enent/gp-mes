package com.mes;

import com.github.pagehelper.PageHelper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;

import java.util.Properties;


@ImportResource("classpath:applicationContext.xml")
@SpringBootApplication(scanBasePackages = "com.mes")
@MapperScan("com.mes.mapper")
public class MesApplication {
    public static void main(String[] args) {
        SpringApplication.run(MesApplication.class,args);
    }


}
