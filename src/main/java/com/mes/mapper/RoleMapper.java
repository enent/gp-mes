package com.mes.mapper;

import com.mes.po.Role;

import java.util.List;

public interface RoleMapper {
    int deleteByPrimaryKey(Integer roleId);

    int insert(Role record);

    int insertSelective(Role record);

    Role selectByPrimaryKey(Integer roleId);

    List<Role> selectBySelective(Integer roleId,String roleName);

    List<Role> selectAllRoles();

    int updateByPrimaryKeySelective(Role record);

    int updateByPrimaryKey(Role record);

    int countRole();
}