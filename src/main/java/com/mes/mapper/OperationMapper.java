package com.mes.mapper;

import com.mes.po.Operation;

public interface OperationMapper extends BaseMapper<Operation>{
    int deleteByPrimaryKey(Integer operationId);

    int insert(Operation record);

    int insertSelective(Operation record);

    Operation selectByPrimaryKey(Integer operationId);

    int updateByPrimaryKeySelective(Operation record);

    int updateByPrimaryKey(Operation record);
}