package com.mes.mapper;

import com.mes.po.Materials;

public interface MaterialsMapper extends BaseMapper<Materials>{
    int deleteByPrimaryKey(Integer materialsId);

    int insert(Materials record);

    int insertSelective(Materials record);

    Materials selectByPrimaryKey(Integer materialsId);

    int updateByPrimaryKeySelective(Materials record);

    int updateByPrimaryKey(Materials record);
}