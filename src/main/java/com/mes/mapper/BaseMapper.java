package com.mes.mapper;

import java.util.List;

public interface BaseMapper<T> {

    int deleteByPrimaryKey(Integer key);

    int insert(T t);

    int insertSelective(T t);

    T selectByPrimaryKey(Integer key);

    int updateByPrimaryKeySelective(T t);

    int updateByPrimaryKey(T t);

    List<T> selectBySelective(Integer id, String name);

    int countItems();

}
