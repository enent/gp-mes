package com.mes.mapper;

import com.mes.po.Crafts;

public interface CraftsMapper extends BaseMapper<Crafts>{
    int deleteByPrimaryKey(Integer craftsId);

    int insert(Crafts record);

    int insertSelective(Crafts record);

    Crafts selectByPrimaryKey(Integer craftsId);

    int updateByPrimaryKeySelective(Crafts record);

    int updateByPrimaryKey(Crafts record);
}