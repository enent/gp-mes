package com.mes.mapper;

import com.mes.po.Privilege;

import java.util.List;

public interface PrivilegeMapper extends BaseMapper<Privilege>{
    int deleteByPrimaryKey(Integer privilegeId);

    int insert(Privilege record);

    int insertSelective(Privilege record);

    Privilege selectByPrimaryKey(Integer privilegeId);

    int updateByPrimaryKeySelective(Privilege record);

    int updateByPrimaryKey(Privilege record);

    List<Privilege> selectAllPrivileges();
}