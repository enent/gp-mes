package com.mes.mapper;

import com.mes.po.User;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer userId);

    List<User> selectAllUsers(Integer userNumber,String userName);

    User selectByUserNum(Integer userNum);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    int countUser();
}