package com.mes.mapper;

import com.mes.po.Product;

import java.util.List;

public interface ProductMapper extends BaseMapper<Product>{
    int deleteByPrimaryKey(Integer productId);

    int insert(Product record);

    int insertSelective(Product record);

    Product selectByPrimaryKey(Integer productId);

    int updateByPrimaryKeySelective(Product record);

    int updateByPrimaryKey(Product record);

    List<Product> selectBySelective(Integer id, String name,String productQuality);

}