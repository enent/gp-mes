package com.mes.mapper;

import com.mes.po.RolePrivilege;

import java.util.List;

public interface RolePrivilegeMapper {
    int deleteByPrimaryKey(Integer rolePrivilegeId);
    int deleteByRoleId(Integer roleId);

    int insert(RolePrivilege record);

    int insertSelective(RolePrivilege record);

    RolePrivilege selectByPrimaryKey(Integer rolePrivilegeId);

    int updateByPrimaryKeySelective(RolePrivilege record);

    int updateByPrimaryKey(RolePrivilege record);

    List<RolePrivilege> selectByRoleId(Integer roleId);
}