package com.mes.controller;

import com.mes.po.Materials;
import com.mes.service.IBaseService;
import com.mes.service.IMaterialsService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/materials")
public class MaterialsController  extends BaseController<Materials> {

    @Autowired
    private IMaterialsService materialsService;
    @Override
    IBaseService<Materials> getService() {
        return materialsService;
    }

    @RequestMapping(value = "/getAllMaterials", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Materials> getAllMaterials(Integer page, Integer limit, Integer materialsId, String materialsCode) {
        return getAllItem(page, limit, materialsId, materialsCode);
    }


    @RequestMapping(value = "/addMaterials", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addMaterials(Materials materials) {
        materials.setCreateTime(new Date());
        return addItem(materials);
    }


    @RequestMapping(value = "/updateMaterials", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateMaterials(Materials materials) {
        return updateItem(materials);
    }

    @RequestMapping(value = "/deleteMaterials", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteMaterials(int materialsId) {
        return deleteItem(materialsId);
    }
}
