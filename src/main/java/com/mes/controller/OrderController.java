package com.mes.controller;

import com.mes.po.Order;
import com.mes.service.IBaseService;
import com.mes.service.IOrderService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/order")
public class OrderController extends BaseController<Order> {

    @Autowired
    private IOrderService orderService;
    @Override
    IBaseService<Order> getService() {
        return orderService;
    }

    @RequestMapping(value = "/getAllOrders", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Order> getAllOrders(Integer page, Integer limit, Integer orderId, String orderCode) {
        return getAllItem(page, limit, orderId, orderCode);
    }


    @RequestMapping(value = "/addOrder", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addOrder(Order order) {
        order.setCreateTime(new Date());
        return addItem(order);
    }


    @RequestMapping(value = "/updateOrder", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateOrder(Order order) {
        return updateItem(order);
    }

    @RequestMapping(value = "/deleteOrder", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteOrder(int orderId) {
        return deleteItem(orderId);
    }
}
