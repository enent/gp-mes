package com.mes.controller;

import com.mes.po.Operation;
import com.mes.service.IBaseService;
import com.mes.service.IOperationService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/operation")
public class OperationController extends BaseController<Operation>{

    @Autowired
    private IOperationService operationService;
    @Override
    IBaseService<Operation> getService() {
        return operationService;
    }

    @RequestMapping(value = "/getAllOperations", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Operation> getAllOperations(Integer page, Integer limit, Integer operationId, String operationName) {
        return getAllItem(page, limit, operationId, operationName);
    }


    @RequestMapping(value = "/addOperation", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addOperation(Operation operation) {
        operation.setCreateTime(new Date());
        return addItem(operation);
    }


    @RequestMapping(value = "/updateOperation", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateOperation(Operation operation) {
        return updateItem(operation);
    }

    @RequestMapping(value = "/deleteOperation", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteOperation(int operationId) {
        return deleteItem(operationId);
    }
}
