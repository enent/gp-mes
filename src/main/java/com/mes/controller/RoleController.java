package com.mes.controller;

import com.mes.constant.ResponseStatus;
import com.mes.mapper.UserRoleMapper;
import com.mes.po.Role;
import com.mes.po.UserRole;
import com.mes.service.IRoleService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserRoleMapper userRoleMapper;

    @RequestMapping(value = "/getAllRoles", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Role> getAllRole(Integer page, Integer limit, Integer roleId, String roleName) {
        TableResponse<Role> response = new TableResponse<Role>(ResponseStatus.SUCESS,"查询成功！");
        List<Role> roleList = roleService.selectAllRoles(page, limit,roleId,roleName);
        Integer countRole = roleService.countRole();
        response.setData(roleList);
        response.setCount(countRole);
        return response;
    }

    @RequestMapping(value = "/getRoles", method = RequestMethod.GET)
    @ResponseBody
    public MesResponse getAllRole() {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"查询成功！");
        List<Role> roleList = roleService.selectRoles();
        response.getData().put("roleList",roleList);
        return response;
    }

    @RequestMapping(value = "/getRolesByUserId", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse getRoleByUserId(Integer userId) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"查询成功！");
        List<UserRole> userRoleList = roleService.selecRoleByUserId(userId);
        response.getData().put("userRoleList",userRoleList);
        return response;
    }


    @RequestMapping(value = "/addRole", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addRole(Role role) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"添加成功");
        int result = roleService.addRole(role);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("添加失败");
        }
        return response;
    }

    @RequestMapping(value = "/updateRole", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateRole(Role role) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"修改成功");
        int result = roleService.updateRole(role);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("修改失败");
        }
        return response;
    }

    @RequestMapping(value = "/deleteRole", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteRole(int roleId) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"删除成功");
        int result = roleService.deleteRole(roleId);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("删除失败");
        }
        return response;
    }


    @RequestMapping(value = "/relationRole", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse relationRole(Integer userId,String roleIds) {
        String[] roleList = roleIds.split(",");
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"添加成功");
        userRoleMapper.deleteByUserId(userId);
        for(String roleId : roleList){
            UserRole userRole = new UserRole();
            userRole.setUserId(userId);
            userRole.setRoleId(Integer.valueOf(roleId));
            userRole.setCreateTime(new Date());
            userRoleMapper.insert(userRole);
        }
        return response;
    }

}
