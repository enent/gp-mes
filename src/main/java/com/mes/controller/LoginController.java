package com.mes.controller;

import com.google.code.kaptcha.Constants;
import com.mes.service.ILoginService;
import com.mes.util.MesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/login")
public class LoginController {

    private static Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private ILoginService loginService;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse login(Integer userNum, String passWord,String code) {
        logger.info("start process login...");
        return loginService.login(userNum,passWord,code);
    }

}
