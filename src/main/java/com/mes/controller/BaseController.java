package com.mes.controller;

import com.mes.constant.ResponseStatus;
import com.mes.service.IBaseService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;

import java.util.List;

public abstract class BaseController<T> {


    abstract IBaseService<T> getService();

    public TableResponse<T> getAllItem(Integer page, Integer limit, Integer roleId, String roleName) {
        TableResponse<T> response = new TableResponse<T>(ResponseStatus.SUCESS, "查询成功！");
        List<T> itemList = this.getService().selectAllRolesByPage(page, limit, roleId, roleName);
        Integer countRole = this.getService().countItem();
        response.setData(itemList);
        response.setCount(countRole);
        return response;
    }


    public MesResponse addItem(T t) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS, "添加成功");
        int result = this.getService().addItem(t);
        if (result != 1) {
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("添加失败");
        }
        return response;
    }


    public MesResponse updateItem(T t) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS, "修改成功");
        int result = this.getService().updateItem(t);
        if (result != 1) {
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("修改失败");
        }
        return response;
    }

    public MesResponse deleteItem(int itemId) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS, "删除成功");
        int result = this.getService().deleteItem(itemId);
        if (result != 1) {
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("删除失败");
        }
        return response;
    }


}
