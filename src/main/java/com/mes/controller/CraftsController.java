package com.mes.controller;

import com.mes.po.Crafts;
import com.mes.service.IBaseService;
import com.mes.service.ICraftsService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/crafts")
public class CraftsController extends BaseController<Crafts>{

    @Autowired
    private ICraftsService craftsService;
    @Override
    IBaseService<Crafts> getService() {
        return craftsService;
    }


    @RequestMapping(value = "/getAllCrafts", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Crafts> getAllCrafts(Integer page, Integer limit, Integer craftsId, String craftsCode) {
        return getAllItem(page, limit, craftsId, craftsCode);
    }


    @RequestMapping(value = "/addCrafts", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addCrafts(Crafts crafts) {
        crafts.setCreateTime(new Date());
        return addItem(crafts);
    }


    @RequestMapping(value = "/updateCrafts", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateCrafts(Crafts crafts) {
        return updateItem(crafts);
    }

    @RequestMapping(value = "/deleteCrafts", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteCrafts(int craftsId) {
        return deleteItem(craftsId);
    }
}
