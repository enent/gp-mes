package com.mes.controller;

import com.mes.constant.MesConstants;
import com.mes.constant.ResponseStatus;
import com.mes.mapper.RolePrivilegeMapper;
import com.mes.po.Privilege;
import com.mes.po.RolePrivilege;
import com.mes.service.IBaseService;
import com.mes.service.IPrivilegeService;
import com.mes.service.IRoleService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import com.mes.vo.UserPriviligers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/privilege")
public class PrivilegeController extends BaseController<Privilege> {

    @Autowired
    private IPrivilegeService privilegeService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private RolePrivilegeMapper rolePrivilegeMapper;

    @RequestMapping(value = "/getAllPrivileges", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<Privilege> getAllPrivileges(Integer page, Integer limit, Integer id, String name) {
        return getAllItem(page, limit, id, name);
    }

    @RequestMapping(value = "/getPrivileges", method = RequestMethod.GET)
    @ResponseBody
    public MesResponse getPrivileges() {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"查询成功！");
        List<Privilege> privilegeList = privilegeService.selectPrivileges();
        response.getData().put("privilegeList",privilegeList);
        return response;
    }

    @RequestMapping(value = "/getPrivilegesByRoleId", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse getPrivilegesByRoleId(Integer roleId) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"查询成功！");
        List<RolePrivilege> rolePrivilegeList = rolePrivilegeMapper.selectByRoleId(roleId);
        response.getData().put("rolePrivilegeList",rolePrivilegeList);
        return response;
    }

    @RequestMapping(value = "/getPrivilegesByUserId", method = RequestMethod.GET)
    @ResponseBody
    public MesResponse getPrivilegesByRoleId() {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"查询成功！");
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Integer userId = Integer.valueOf(request.getSession().getAttribute(MesConstants.userId).toString());
        UserPriviligers userPriviligers = privilegeService.getPrivilegesByUserId(userId);
        response.getData().put("userPriviligers",userPriviligers);
        return response;
    }


    @RequestMapping(value = "/addPrivilege", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addPrivilege(Privilege privilege) {
        return addItem(privilege);
    }


    @RequestMapping(value = "/updatePrivilege", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updatePrivilege(Privilege privilege) {
        return updateItem(privilege);
    }

    @RequestMapping(value = "/deletePrivilege", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deletePrivilege(int privilegeId) {
        return deleteItem(privilegeId);
    }

    @Override
    IBaseService getService() {
        return privilegeService;
    }


    @RequestMapping(value = "/relationPrivilege", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse relationPrivilege(Integer roleId, String privilegeIds) {
        String[] privilegeIdList = privilegeIds.split(",");
        rolePrivilegeMapper.deleteByRoleId(roleId);
        for(String privilegeId : privilegeIdList){
            RolePrivilege rolePrivilege = new RolePrivilege();
            rolePrivilege.setRoleId(roleId);
            rolePrivilege.setPrivilegeId(Integer.valueOf(privilegeId));
            rolePrivilege.setCreateTime(new Date());
            rolePrivilegeMapper.insert(rolePrivilege);
        }
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"添加成功");
        return response;
    }

}
