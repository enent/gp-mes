package com.mes.controller;

import com.mes.constant.ResponseStatus;
import com.mes.mapper.UserMapper;
import com.mes.po.User;
import com.mes.service.IUserService;
import com.mes.util.MesResponse;
import com.mes.util.TableResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserMapper userMapper;

    @RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
    @ResponseBody
    public TableResponse<User> getAllUsers(Integer page, Integer limit,Integer userNumber,String userName) {
        TableResponse<User> response = new TableResponse<User>();
        List<User> userList = userService.selectAllUsers(page, limit,userNumber,userName);
        Integer countUser = userService.countUser();
        response.setCode(ResponseStatus.SUCESS);
        response.setData(userList);
        response.setCount(countUser);
        response.setMessage("查询成功！");
        return response;
    }


    @RequestMapping(value = "/addUser", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse addUser(User user) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"添加成功");
        int result = userService.addUser(user);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("添加失败");
        }
        return response;
    }

    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updateUser(User user) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"修改成功");
        int result = userService.updateUser(user);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("修改失败");
        }
        return response;
    }

    @RequestMapping(value = "/deleteUser", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse deleteUser(int userId) {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"删除成功");
        int result = userService.deleteUser(userId);
        if(result != 1){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("删除失败");
        }
        return response;
    }



    @RequestMapping(value = "/getCurrentUser", method = RequestMethod.GET)
    @ResponseBody
    public MesResponse getCurrentUser() {
        MesResponse response = new MesResponse(ResponseStatus.SUCESS,"添加成功");
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        String userId = request.getSession().getAttribute("userId").toString();
        if(null == userId){
            response.setStatus(ResponseStatus.FAILED);
            response.setMessage("系统异常，获取数据失败！");
            return response;
        }
        User user = userMapper.selectByPrimaryKey(Integer.valueOf(userId));
        response.getData().put("user",user);
        return response;
    }

    @RequestMapping(value = "/updatePassword", method = RequestMethod.POST)
    @ResponseBody
    public MesResponse updatePassword(Integer userId,String oldPassword,String newPassword) {
        MesResponse response = new MesResponse();
        response.setStatus(ResponseStatus.FAILED);
        User user = userMapper.selectByPrimaryKey(userId);
        if(null == user){
            response.setMessage("系统异常，用户不存在！");
            return response;
        }
        if(!user.getPassword().equals(oldPassword)){
            response.setMessage("旧密码输入错误！");
            return response;
        }
        user.setPassword(newPassword);
        int result = userService.updateUser(user);
        if(result != 1){
            response.setMessage("修改失败");
            return response;
        }
        response.setStatus(ResponseStatus.SUCESS);
        response.setMessage("密码修改成功！");
        return response;
    }

}
