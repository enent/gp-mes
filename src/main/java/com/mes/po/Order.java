package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/23
 */
public class Order {
    /**
     * 
     */
    private Integer orderId;

    /**
     * 
     */
    private String orderCode;

    /**
     * 
     */
    private Integer productId;

    /**
     * 
     */
    private Integer productNum;

    /**
     * 
     */
    private String orderStatus;

    /**
     * 
     */
    private String createBy;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date endTime;

    /**
     * 
     */
    private String site;

    public Order(Integer orderId, String orderCode, Integer productId, Integer productNum, String orderStatus, String createBy, Date createTime, Date endTime, String site) {
        this.orderId = orderId;
        this.orderCode = orderCode;
        this.productId = productId;
        this.productNum = productNum;
        this.orderStatus = orderStatus;
        this.createBy = createBy;
        this.createTime = createTime;
        this.endTime = endTime;
        this.site = site;
    }

    public Order() {
        super();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getProductNum() {
        return productNum;
    }

    public void setProductNum(Integer productNum) {
        this.productNum = productNum;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus == null ? null : orderStatus.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", orderId=").append(orderId);
        sb.append(", orderCode=").append(orderCode);
        sb.append(", productId=").append(productId);
        sb.append(", productNum=").append(productNum);
        sb.append(", orderStatus=").append(orderStatus);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", site=").append(site);
        sb.append("]");
        return sb.toString();
    }
}