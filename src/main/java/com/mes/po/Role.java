package com.mes.po;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class Role {
    /**
     * 
     */
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String reloDesc;

    /**
     * 
     */
    private Integer roleStatus;

    public Role(Integer roleId, String roleName, String reloDesc, Integer roleStatus) {
        this.roleId = roleId;
        this.roleName = roleName;
        this.reloDesc = reloDesc;
        this.roleStatus = roleStatus;
    }

    public Role() {
        super();
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName == null ? null : roleName.trim();
    }

    public String getReloDesc() {
        return reloDesc;
    }

    public void setReloDesc(String reloDesc) {
        this.reloDesc = reloDesc == null ? null : reloDesc.trim();
    }

    public Integer getRoleStatus() {
        return roleStatus;
    }

    public void setRoleStatus(Integer roleStatus) {
        this.roleStatus = roleStatus;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", roleId=").append(roleId);
        sb.append(", roleName=").append(roleName);
        sb.append(", reloDesc=").append(reloDesc);
        sb.append(", roleStatus=").append(roleStatus);
        sb.append("]");
        return sb.toString();
    }
}