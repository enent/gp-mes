package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/23
 */
public class Materials {
    /**
     * 
     */
    private Integer materialsId;

    /**
     * 
     */
    private String materialsCode;

    /**
     * 
     */
    private String materialsName;

    /**
     * 
     */
    private String materialsDesc;

    /**
     * 
     */
    private Integer materialsNum;

    /**
     * 
     */
    private String materialsType;

    /**
     * 
     */
    private String createBy;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private String site;

    public Materials(Integer materialsId, String materialsCode, String materialsName, String materialsDesc, Integer materialsNum, String materialsType, String createBy, Date createTime, String site) {
        this.materialsId = materialsId;
        this.materialsCode = materialsCode;
        this.materialsName = materialsName;
        this.materialsDesc = materialsDesc;
        this.materialsNum = materialsNum;
        this.materialsType = materialsType;
        this.createBy = createBy;
        this.createTime = createTime;
        this.site = site;
    }

    public Materials() {
        super();
    }

    public Integer getMaterialsId() {
        return materialsId;
    }

    public void setMaterialsId(Integer materialsId) {
        this.materialsId = materialsId;
    }

    public String getMaterialsCode() {
        return materialsCode;
    }

    public void setMaterialsCode(String materialsCode) {
        this.materialsCode = materialsCode == null ? null : materialsCode.trim();
    }

    public String getMaterialsName() {
        return materialsName;
    }

    public void setMaterialsName(String materialsName) {
        this.materialsName = materialsName == null ? null : materialsName.trim();
    }

    public String getMaterialsDesc() {
        return materialsDesc;
    }

    public void setMaterialsDesc(String materialsDesc) {
        this.materialsDesc = materialsDesc == null ? null : materialsDesc.trim();
    }

    public Integer getMaterialsNum() {
        return materialsNum;
    }

    public void setMaterialsNum(Integer materialsNum) {
        this.materialsNum = materialsNum;
    }

    public String getMaterialsType() {
        return materialsType;
    }

    public void setMaterialsType(String materialsType) {
        this.materialsType = materialsType == null ? null : materialsType.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", materialsId=").append(materialsId);
        sb.append(", materialsCode=").append(materialsCode);
        sb.append(", materialsName=").append(materialsName);
        sb.append(", materialsDesc=").append(materialsDesc);
        sb.append(", materialsNum=").append(materialsNum);
        sb.append(", materialsType=").append(materialsType);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", site=").append(site);
        sb.append("]");
        return sb.toString();
    }
}