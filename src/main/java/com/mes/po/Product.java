package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/23
 */
public class Product {
    /**
     * 
     */
    private Integer productId;

    /**
     * 
     */
    private Integer orderId;

    /**
     * 
     */
    private Integer materialsId;

    /**
     * 
     */
    private String productCode;

    /**
     * 
     */
    private Integer materialsNum;

    /**
     * 
     */
    private String productStatus;

    /**
     * 
     */
    private String productQuality;

    /**
     * 
     */
    private String createBy;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private String site;

    public Product(Integer productId, Integer orderId, Integer materialsId, String productCode, Integer materialsNum, String productStatus, String productQuality, String createBy, Date createTime, String site) {
        this.productId = productId;
        this.orderId = orderId;
        this.materialsId = materialsId;
        this.productCode = productCode;
        this.materialsNum = materialsNum;
        this.productStatus = productStatus;
        this.productQuality = productQuality;
        this.createBy = createBy;
        this.createTime = createTime;
        this.site = site;
    }

    public Product() {
        super();
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getMaterialsId() {
        return materialsId;
    }

    public void setMaterialsId(Integer materialsId) {
        this.materialsId = materialsId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode == null ? null : productCode.trim();
    }

    public Integer getMaterialsNum() {
        return materialsNum;
    }

    public void setMaterialsNum(Integer materialsNum) {
        this.materialsNum = materialsNum;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus == null ? null : productStatus.trim();
    }

    public String getProductQuality() {
        return productQuality;
    }

    public void setProductQuality(String productQuality) {
        this.productQuality = productQuality == null ? null : productQuality.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", productId=").append(productId);
        sb.append(", orderId=").append(orderId);
        sb.append(", materialsId=").append(materialsId);
        sb.append(", productCode=").append(productCode);
        sb.append(", materialsNum=").append(materialsNum);
        sb.append(", productStatus=").append(productStatus);
        sb.append(", productQuality=").append(productQuality);
        sb.append(", createBy=").append(createBy);
        sb.append(", createTime=").append(createTime);
        sb.append(", site=").append(site);
        sb.append("]");
        return sb.toString();
    }
}