package com.mes.po;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class Privilege {
    /**
     * 
     */
    private Integer privilegeId;

    /**
     * 
     */
    private String privilegeName;

    /**
     * 
     */
    private Integer parentId;

    /**
     * 链接地址
     */
    private String url;

    /**
     * 跳转地址
     */
    private String target;

    /**
     * 权限描述
     */
    private String privilegeDesc;

    /**
     * 
     */
    private Integer status;

    public Privilege(Integer privilegeId, String privilegeName, Integer parentId, String url, String target, String privilegeDesc, Integer status) {
        this.privilegeId = privilegeId;
        this.privilegeName = privilegeName;
        this.parentId = parentId;
        this.url = url;
        this.target = target;
        this.privilegeDesc = privilegeDesc;
        this.status = status;
    }

    public Privilege() {
        super();
    }

    public Integer getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Integer privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getPrivilegeName() {
        return privilegeName;
    }

    public void setPrivilegeName(String privilegeName) {
        this.privilegeName = privilegeName == null ? null : privilegeName.trim();
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url == null ? null : url.trim();
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target == null ? null : target.trim();
    }

    public String getPrivilegeDesc() {
        return privilegeDesc;
    }

    public void setPrivilegeDesc(String privilegeDesc) {
        this.privilegeDesc = privilegeDesc == null ? null : privilegeDesc.trim();
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", privilegeId=").append(privilegeId);
        sb.append(", privilegeName=").append(privilegeName);
        sb.append(", parentId=").append(parentId);
        sb.append(", url=").append(url);
        sb.append(", target=").append(target);
        sb.append(", privilegeDesc=").append(privilegeDesc);
        sb.append(", status=").append(status);
        sb.append("]");
        return sb.toString();
    }
}