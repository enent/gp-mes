package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class Operation {
    /**
     * 
     */
    private Integer operationId;

    /**
     * 操作编号
     */
    private String operationCode;

    /**
     * 操作描述
     */
    private String operationDesc;

    /**
     * 
     */
    private String site;

    /**
     * 
     */
    private String createBy;

    /**
     * 0为按键，1为旋钮，2为语音
     */
    private Integer operationType;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 
     */
    private Date createTime;

    public Operation(Integer operationId, String operationCode, String operationDesc, String site, String createBy, Integer operationType, Date updateTime, Date createTime) {
        this.operationId = operationId;
        this.operationCode = operationCode;
        this.operationDesc = operationDesc;
        this.site = site;
        this.createBy = createBy;
        this.operationType = operationType;
        this.updateTime = updateTime;
        this.createTime = createTime;
    }

    public Operation() {
        super();
    }

    public Integer getOperationId() {
        return operationId;
    }

    public void setOperationId(Integer operationId) {
        this.operationId = operationId;
    }

    public String getOperationCode() {
        return operationCode;
    }

    public void setOperationCode(String operationCode) {
        this.operationCode = operationCode == null ? null : operationCode.trim();
    }

    public String getOperationDesc() {
        return operationDesc;
    }

    public void setOperationDesc(String operationDesc) {
        this.operationDesc = operationDesc == null ? null : operationDesc.trim();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Integer getOperationType() {
        return operationType;
    }

    public void setOperationType(Integer operationType) {
        this.operationType = operationType;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", operationId=").append(operationId);
        sb.append(", operationCode=").append(operationCode);
        sb.append(", operationDesc=").append(operationDesc);
        sb.append(", site=").append(site);
        sb.append(", createBy=").append(createBy);
        sb.append(", operationType=").append(operationType);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}