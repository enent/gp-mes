package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class Crafts {
    /**
     * 
     */
    private Integer craftsId;

    /**
     * 工艺编号
     */
    private String craftsCode;

    /**
     * 工艺描述
     */
    private String craftsDesc;

    /**
     * 
     */
    private String site;

    /**
     * 
     */
    private String createBy;

    /**
     * 0为标准工艺，1为专业工艺
     */
    private Integer craftsType;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 
     */
    private Date createTime;

    public Crafts(Integer craftsId, String craftsCode, String craftsDesc, String site, String createBy, Integer craftsType, Date updateTime, Date createTime) {
        this.craftsId = craftsId;
        this.craftsCode = craftsCode;
        this.craftsDesc = craftsDesc;
        this.site = site;
        this.createBy = createBy;
        this.craftsType = craftsType;
        this.updateTime = updateTime;
        this.createTime = createTime;
    }

    public Crafts() {
        super();
    }

    public Integer getCraftsId() {
        return craftsId;
    }

    public void setCraftsId(Integer craftsId) {
        this.craftsId = craftsId;
    }

    public String getCraftsCode() {
        return craftsCode;
    }

    public void setCraftsCode(String craftsCode) {
        this.craftsCode = craftsCode == null ? null : craftsCode.trim();
    }

    public String getCraftsDesc() {
        return craftsDesc;
    }

    public void setCraftsDesc(String craftsDesc) {
        this.craftsDesc = craftsDesc == null ? null : craftsDesc.trim();
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site == null ? null : site.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Integer getCraftsType() {
        return craftsType;
    }

    public void setCraftsType(Integer craftsType) {
        this.craftsType = craftsType;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", craftsId=").append(craftsId);
        sb.append(", craftsCode=").append(craftsCode);
        sb.append(", craftsDesc=").append(craftsDesc);
        sb.append(", site=").append(site);
        sb.append(", createBy=").append(createBy);
        sb.append(", craftsType=").append(craftsType);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append("]");
        return sb.toString();
    }
}