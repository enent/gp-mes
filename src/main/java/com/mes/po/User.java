package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class User {
    /**
     * 
     */
    private Integer userId;

    /**
     * 用户编号，用于登录系统
     */
    private Integer userNumber;

    /**
     * 
     */
    private String password;

    /**
     * 
     */
    private String userName;

    /**
     * 0为失效，1为正常，2为限制
     */
    private Integer userStatus;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 
     */
    private Date createTime;

    private String userEmail;

    private String remarks;

    public User(Integer userId, Integer userNumber, String password, String userName,
                Integer userStatus, Date updateTime, Date createTime,String userEmail,String remarks) {
        this.userId = userId;
        this.userNumber = userNumber;
        this.password = password;
        this.userName = userName;
        this.userStatus = userStatus;
        this.updateTime = updateTime;
        this.createTime = createTime;
        this.userEmail = userEmail;
        this.remarks = remarks;
    }

    public User() {
        super();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(Integer userNumber) {
        this.userNumber = userNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", userId=").append(userId);
        sb.append(", userNumber=").append(userNumber);
        sb.append(", password=").append(password);
        sb.append(", userName=").append(userName);
        sb.append(", userStatus=").append(userStatus);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", createTime=").append(createTime);
        sb.append(", userEmail=").append(userEmail);
        sb.append(", remarks=").append(remarks);
        sb.append("]");
        return sb.toString();
    }
}