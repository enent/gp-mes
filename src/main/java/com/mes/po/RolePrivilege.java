package com.mes.po;

import java.util.Date;

/**
 * 
 * @author Mo
 * @date 2020/04/29
 */
public class RolePrivilege {
    /**
     * 
     */
    private Integer rolePrivilegeId;

    /**
     * 
     */
    private Integer roleId;

    /**
     * 
     */
    private Integer privilegeId;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

    /**
     * 1为正常，0为异常
     */
    private Integer status;

    public RolePrivilege(Integer rolePrivilegeId, Integer roleId, Integer privilegeId, Date createTime, Date updateTime, Integer status) {
        this.rolePrivilegeId = rolePrivilegeId;
        this.roleId = roleId;
        this.privilegeId = privilegeId;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.status = status;
    }

    public RolePrivilege() {
        super();
    }

    public Integer getRolePrivilegeId() {
        return rolePrivilegeId;
    }

    public void setRolePrivilegeId(Integer rolePrivilegeId) {
        this.rolePrivilegeId = rolePrivilegeId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Integer privilegeId) {
        this.privilegeId = privilegeId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", rolePrivilegeId=").append(rolePrivilegeId);
        sb.append(", roleId=").append(roleId);
        sb.append(", privilegeId=").append(privilegeId);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", status=").append(status);
        sb.append("]");
        return sb.toString();
    }
}