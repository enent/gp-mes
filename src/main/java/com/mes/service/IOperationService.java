package com.mes.service;

import com.mes.po.Operation;

public interface IOperationService extends IBaseService<Operation> {
}
