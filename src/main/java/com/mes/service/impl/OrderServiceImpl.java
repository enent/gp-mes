package com.mes.service.impl;

import com.mes.mapper.BaseMapper;
import com.mes.mapper.OrderMapper;
import com.mes.po.Order;
import com.mes.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl extends BaseServiceImpl<Order> implements IOrderService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private OrderMapper orderMapper;

    @Override
    BaseMapper<Order> getMapper() {
        return orderMapper;
    }
}
