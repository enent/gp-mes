package com.mes.service.impl;

import com.github.pagehelper.PageHelper;
import com.mes.mapper.RoleMapper;
import com.mes.mapper.UserRoleMapper;
import com.mes.po.Role;
import com.mes.po.UserRole;
import com.mes.service.IRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements IRoleService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<Role> selectAllRoles(Integer currentPage, Integer pageSize, Integer roleId, String roleName) {
        PageHelper.startPage(currentPage, pageSize);
        List<Role>  roleList = roleMapper.selectBySelective(roleId,roleName);
        return roleList;
    }

    @Override
    public int addRole(Role role) {
        return roleMapper.insertSelective(role);
    }

    @Override
    public int updateRole(Role role) {
        return roleMapper.updateByPrimaryKeySelective(role);
    }

    @Override
    public int deleteRole(int roleId) {
        return roleMapper.deleteByPrimaryKey(roleId);
    }

    @Override
    public int countRole() {
        return roleMapper.countRole();
    }

    @Override
    public List<Role> selectRoles() {
        return roleMapper.selectAllRoles();
    }

    @Override
    public List<UserRole> selecRoleByUserId(Integer userId) {
        return userRoleMapper.selectByUserId(userId);
    }
}
