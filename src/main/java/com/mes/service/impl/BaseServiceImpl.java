package com.mes.service.impl;

import com.github.pagehelper.PageHelper;
import com.mes.mapper.BaseMapper;
import com.mes.service.IBaseService;

import java.util.List;


public abstract class BaseServiceImpl<T> implements IBaseService<T> {


    abstract BaseMapper<T> getMapper();

    @Override
    public List<T> selectAllRolesByPage(Integer currentPage, Integer pageSize, Integer itemId, String itemName) {
        PageHelper.startPage(currentPage, pageSize);
        List<T> itemList = this.getMapper().selectBySelective(itemId, itemName);
        return itemList;
    }

    @Override
    public int addItem(T t) {
        return this.getMapper().insertSelective(t);
    }

    @Override
    public int updateItem(T t) {
        return this.getMapper().updateByPrimaryKeySelective(t);
    }

    @Override
    public int deleteItem(int itemId) {
        return this.getMapper().deleteByPrimaryKey(itemId);
    }

    @Override
    public int countItem() {
        return this.getMapper().countItems();
    }
}
