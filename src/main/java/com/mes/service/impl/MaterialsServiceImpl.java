package com.mes.service.impl;

import com.mes.mapper.BaseMapper;
import com.mes.mapper.MaterialsMapper;
import com.mes.po.Materials;
import com.mes.service.IMaterialsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MaterialsServiceImpl extends BaseServiceImpl<Materials> implements IMaterialsService {

    @Autowired
    private MaterialsMapper materialsMapper;
    @Override
    BaseMapper<Materials> getMapper() {
        return materialsMapper;
    }
}
