package com.mes.service.impl;

import com.github.pagehelper.PageHelper;
import com.mes.mapper.BaseMapper;
import com.mes.mapper.ProductMapper;
import com.mes.po.Product;
import com.mes.service.IProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl extends BaseServiceImpl<Product> implements IProductService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private ProductMapper productMapper;
    @Override
    BaseMapper<Product> getMapper() {
        return productMapper;
    }

    @Override
    public List<Product> selectProductByQuality(Integer currentPage, Integer pageSize, Integer itemId,
                                                String itemName, String productQuality) {
        PageHelper.startPage(currentPage, pageSize);
        List<Product> itemList = productMapper.selectBySelective(itemId, itemName,productQuality);
        return itemList;
    }
}
