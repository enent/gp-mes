package com.mes.service.impl;

import com.github.pagehelper.PageHelper;
import com.mes.mapper.UserMapper;
import com.mes.po.User;
import com.mes.service.IUserService;
import com.mes.util.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private UserMapper userMapper;

    @Override
    public List<User> selectAllUsers(Integer currentPage, Integer pageSize,Integer userNumber,String userName) {
        PageHelper.startPage(currentPage, pageSize);
        List<User> userList = userMapper.selectAllUsers(userNumber,userName);
        return userList;
    }

    @Override
    public int addUser(User user) {
        user.setCreateTime(new Date());
        return userMapper.insertSelective(user);
    }

    @Override
    public int updateUser(User user) {
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public int deleteUser(int userId) {
        return userMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int countUser() {
        return userMapper.countUser();
    }
}
