package com.mes.service.impl;

import com.mes.mapper.BaseMapper;
import com.mes.mapper.CraftsMapper;
import com.mes.po.Crafts;
import com.mes.service.ICraftsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class CraftsServiceImpl extends BaseServiceImpl<Crafts> implements ICraftsService {

    @Autowired
    private CraftsMapper craftsMapper;
    @Override
    BaseMapper<Crafts> getMapper() {
        return craftsMapper;
    }
}
