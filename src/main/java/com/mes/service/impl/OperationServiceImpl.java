package com.mes.service.impl;

import com.mes.mapper.BaseMapper;
import com.mes.mapper.OperationMapper;
import com.mes.po.Operation;
import com.mes.service.IOperationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OperationServiceImpl extends BaseServiceImpl<Operation> implements IOperationService {

    @Autowired
    private OperationMapper operationMapper;
    @Override
    BaseMapper<Operation> getMapper() {
        return operationMapper;
    }
}
