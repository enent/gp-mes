package com.mes.service;

import com.mes.po.Privilege;
import com.mes.vo.UserPriviligers;

import java.util.List;
import java.util.Map;

public interface IPrivilegeService extends IBaseService<Privilege> {

    List<Privilege> selectPrivileges();

    UserPriviligers getPrivilegesByUserId(Integer userId);
}
