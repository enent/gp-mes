package com.mes.service;

import java.util.List;

public interface IBaseService<T> {
    List<T> selectAllRolesByPage(Integer currentPage, Integer pageSize, Integer itemId, String itemName);

    int addItem(T t);

    int updateItem(T t);

    int deleteItem(int itemId);

    int countItem();
}
