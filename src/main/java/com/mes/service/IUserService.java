package com.mes.service;

import com.mes.po.User;

import java.util.List;

public interface IUserService {

    List<User> selectAllUsers(Integer page, Integer limit,Integer userNumber,String userName);

    int addUser(User user);

    int updateUser(User user);

    int deleteUser(int userNum);

    int countUser();
}
