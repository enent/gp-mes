package com.mes.service;

import com.mes.util.MesResponse;

public interface ILoginService {

    MesResponse login(Integer userNum, String password,String code);
}
