package com.mes.service;

import com.mes.po.Product;

import java.util.List;

public interface IProductService extends IBaseService<Product>{

    List<Product> selectProductByQuality(Integer currentPage, Integer pageSize, Integer itemId,
                                       String itemName,String productQuality);

}
