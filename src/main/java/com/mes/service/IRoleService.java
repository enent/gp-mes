package com.mes.service;

import com.mes.po.Role;
import com.mes.po.UserRole;

import java.util.List;

public interface IRoleService {

    List<Role> selectAllRoles(Integer page, Integer limit, Integer RoleNumber, String RoleName);

    int addRole(Role role);

    int updateRole(Role role);

    int deleteRole(int roleId);

    int countRole();

    List<Role> selectRoles();
    List<UserRole> selecRoleByUserId(Integer userId);


}
