package com.mes.vo;

import com.mes.po.Privilege;

import java.util.List;
import java.util.Map;

public class UserPriviligers {

    private List<Privilege> privileges;

    private Map<String,List<Privilege>> privilegeMap;

    public List<Privilege> getPrivileges() {
        return privileges;
    }

    public void setPrivileges(List<Privilege> privileges) {
        this.privileges = privileges;
    }

    public Map<String, List<Privilege>> getPrivilegeMap() {
        return privilegeMap;
    }

    public void setPrivilegeMap(Map<String, List<Privilege>> privilegeMap) {
        this.privilegeMap = privilegeMap;
    }
}
