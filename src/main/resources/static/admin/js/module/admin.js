var user = null;
layui.use(['form','element'], function(){
    var form = layui.form;
    var element = layui.element;
    form.render();
    //监听信息提交
    form.on('submit(adminInfo)', function(data){
        layer.msg(JSON.stringify(data.field));
        return false;
    });
    //监听密码提交
    form.on('submit(adminPassword)', function(data){
        if(data.field.newPassword != data.field.confirmPassword){
            layer.alert("新密码两次输入不一致！请检查");
        }
        return false;
    });

    form.on('submit(adminInfo)', function(data) {
        var userData = form.val("admin-info");
        $.ajax({
            type: "post",
            data: userData,
            async:false,
            url: "http://localhost:8080/user/updateUser",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统异常，请联系管理员!");
            },
            success: function (data) {
                layer.alert(data.message, {
                    skin: 'layui-layer-molv' //样式类名  自定义样式
                    , closeBtn: 1    // 是否显示关闭按钮
                    , anim: 1 //动画类型
                    , icon: 6    // icon
                });
            }
        });
        return false;
    });

    form.on('submit(adminPassword)', function(data) {
        $.ajax({
            type: "post",
            data: {"userId": user.userId,"oldPassword":data.field.oldPassword,"newPassword":data.field.newPassword},
            async:false,
            url: "http://localhost:8080/user/updatePassword",
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("系统异常，请联系管理员!");
            },
            success: function (data) {
                layer.alert(data.message, {
                    skin: 'layui-layer-molv' //样式类名  自定义样式
                    , closeBtn: 1    // 是否显示关闭按钮
                    , anim: 1 //动画类型
                    , icon: 6    // icon
                });
            }
        });
        return false;
    });

    var $ = layui.$;
    $.ajax({
        type: "get",
        url: "http://localhost:8080/user/getCurrentUser",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统异常，请联系管理员!");
        },
        success: function (data) {
            user = data.data.user;
            form.val("admin-info", user);
            form.val("admin-password",{
                "userName":user.userName
            });
        }
    })
});