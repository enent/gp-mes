var editCrafts = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#crafts-table'
        , url: 'http://localhost:8080/crafts/getAllCrafts'
        , cols: [[
            {field: 'craftsId', title: 'ID',  sort: true}
            , {field: 'craftsCode',  title: '工艺编号'}
            , {field: 'craftsDesc',  title: '工艺描述'}
            , {field: 'site',  title: '站点'}
            , {field: 'craftsType',  title: '工艺类型',templet: function (crafts) {
                    if (crafts.craftsType == '0') {
                        return '标准';
                    } else if (crafts.craftsType == '1') {
                        return '专业';
                    }
                }}
            , {field: 'createBy',  title: '创建人'}
            , {field: 'createTime',  title: '创建时间',templet: function (crafts) {
                    return layui.util.toDateString(crafts.createTime, 'yyyy-MM-dd')
                }}
            , {field: 'updateTime', title: '更新时间',templet: function (crafts) {
                    return layui.util.toDateString(crafts.updateTime, 'yyyy-MM-dd')
                }}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'craftsTable'
    });

    table.on('tool(crafts-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该操作吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"craftsId": data.craftsId},
                    url: "http://localhost:8080/crafts/deleteCrafts",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editCrafts = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/crafts-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var craftsId = $('#craftsId');
            var craftsCode = $('#craftsCode');
            //执行重载
            table.reload('craftsTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    craftsId: craftsId.val(),
                    craftsCode: craftsCode.val()
                }
            }, 'data');
        }
    };

    $('#reloadCrafts').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


