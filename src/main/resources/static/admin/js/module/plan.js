var editPlan = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#plan-table'
        , url: 'http://localhost:8080/order/getAllOrders'
        , cols: [[
            {field: 'orderId', title: 'ID',  sort: true}
            , {field: 'orderCode',  title: '计划编号'}
            , {field: 'productId',  title: '产品ID'}
            , {field: 'productNum',  title: '剩余产品数量'}
            , {field: 'orderStatus',  title: '计划状态',templet: function (order) {
                    if (order.orderStatus == '0') {
                        return '未执行';
                    } else if (order.orderStatus == '1') {
                        return '执行中';
                    } else if (order.orderStatus == '2') {
                        return '返工中';
                    }else if (order.orderStatus == '3') {
                        return '已完成';
                    }else if (order.orderStatus == '4') {
                        return '已取消';
                    }
                }}
            , {field: 'createBy',  title: '创建人'}
            , {field: 'site', title: '站点'}
            , {field: 'createTime',  title: '创建时间',templet: function (order) {
                    return layui.util.toDateString(order.createTime, 'yyyy-MM-dd')
                }}
            , {field: 'endTime',  title: '终止时间',templet: function (order) {
                    return layui.util.toDateString(order.endTime, 'yyyy-MM-dd')
                }}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'planTable'
    });

    table.on('tool(plan-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该计划吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"orderId": data.orderId},
                    url: "http://localhost:8080/order/deleteOrder",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editPlan = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/plan-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var orderId = $('#orderId');
            var orderCode = $('#orderCode');
            //执行重载
            table.reload('planTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    orderId: orderId.val(),
                    orderCode: orderCode.val()
                }
            }, 'data');
        }
    };

    $('#reloadPlan').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


