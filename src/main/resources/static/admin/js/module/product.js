var editProduct = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#product-table'
        , url: 'http://localhost:8080/product/getAllProducts?productQuality=0'
        , cols: [[
            {field: 'productId', title: 'ID',  sort: true}
            , {field: 'productCode',  title: '产品编号'}
            , {field: 'orderId', title: '计划ID'}
            , {field: 'materialsId',  title: '物料ID'}
            , {field: 'materialsNum', title: '使用物料数量'}
            , {field: 'productStatus',  title: '产品状态',templet: function (product) {
                    if (product.productStatus == '0') {
                        return '已完成';
                    } else if (product.productStatus == '1') {
                        return '制造中';
                    }
                }}
            , {field: 'productQuality',  title: '产品质量', hide:true}
            , {field: 'createBy', title: '负责人'}
            , {field: 'createTime', title: '创建时间',templet: function (product) {
                    return layui.util.toDateString(product.createTime, 'yyyy-MM-dd')
                }}
            , {field: 'site',  title: '站点'}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'productTable'
    });

    table.on('tool(product-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该用户吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"productId": data.productId},
                    url: "http://localhost:8080/product/deleteProduct",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editProduct = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "520px"],
                fixed: false, //不固定
                content: "../index/product-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var productId = $('#productId');
            var productCode = $('#productCode');
            //执行重载
            table.reload('productTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    productId: productId.val(),
                    productCode: productCode.val()
                }
            }, 'data');
        }
    };

    $('#reloadProduct').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


