var editOperation = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#operation-table'
        , url: 'http://localhost:8080/operation/getAllOperations'
        , cols: [[
            {field: 'operationId', title: 'ID', width: 80, sort: true}
            , {field: 'operationCode', width: 120, title: '操作编号'}
            , {field: 'operationDesc', width: 120, title: '操作描述'}
            , {field: 'site', width: 100, title: '站点'}
            , {field: 'operationType', width: 100, title: '操作类型',templet: function (operation) {
                    if (operation.operationType == '0') {
                        return '按键';
                    } else if (operation.operationType == '1') {
                        return '旋钮';
                    } else if(operation.operationType == '2'){
                        return '语音';
                    }
                }}
            , {field: 'createBy', width: 100, title: '创建人'}
            , {field: 'createTime', width: 200, title: '创建时间', templet: function (operation) {
                    return layui.util.toDateString(operation.createTime, 'yyyy-MM-dd')
                }}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'operationTable'
    });

    table.on('tool(operation-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该操作吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"operationId": data.operationId},
                    url: "http://localhost:8080/operation/deleteOperation",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editOperation = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/operation-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var operationId = $('#operationId');
            var operationName = $('#operationCode');
            //执行重载
            table.reload('operationTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    operationId: operationId.val(),
                    operationName: operationName.val()
                }
            }, 'data');
        }
    };

    $('#reloadOperation').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


