var editRole = null ;
var roleId = null;
var privilegeList = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#role-table'
        , url: 'http://localhost:8080/role/getAllRoles'
        , cols: [[
            {field: 'roleId', title: '序号', width: 80, sort: true}
            , {field: 'roleId', width: 120, title: '角色ID'}
            , {field: 'roleName', width: 120, title: '角色名称'}
            , {field: 'roleStatus', width: 100, title: '角色状态',templet: function (role) {
                    if (role.roleStatus == '0') {
                        return '正常';
                    } else if (role.roleStatus == '1') {
                        return '无效';
                    }
                }}
            , {field: 'reloDesc', width: 100, title: '角色描述'}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barRole'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'roleTable'
    });

    table.on('tool(role-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该角色吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"roleId": data.roleId},
                    url: "http://localhost:8080/role/deleteRole",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            editRole = data;
            var index = layer.open({
                type: 2,
                title: "角色修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/role-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        } else if (obj.event === 'relationPrivilege'){
            roleId = data.roleId;
            layer.open({
                type: 2
                , title: "分配权限"
                , skin: 'layui-layer-molv'
                , area: ["500px", "420px"]
                , content:"../index/role-privilege.html"
                ,sucess:function(layero, index){
                }
            })
        }
    });


        var $ = layui.$, active = {
        reload: function () {
            var roleId = $('#roleId');
            var roleName = $('#roleName');
            //执行重载
            table.reload('roleTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    roleId: roleId.val(),
                    roleName: roleName.val()
                }
            }, 'data');
        }
    };

    $('#reloadRole').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    $.ajax({
        type: "get",
        url: "http://localhost:8080/privilege/getPrivileges",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统异常，请联系管理员!");
        },
        success: function (data) {
            privilegeList = data.data.privilegeList;
        }
    })

});


