var editPrivilege = null ;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#privilege-table'
        , url: 'http://localhost:8080/privilege/getAllPrivileges'
        , cols: [[
             {field: 'privilegeId', width: 120, title: '权限ID'}
            , {field: 'privilegeName', width: 120, title: '权限名称'}
            , {field: 'parentId', width: 120, title: '父ID'}
            , {field: 'url', width: 100, title: '权限链接'}
            , {field: 'target', width: 100, title: '权限图案'}
            , {field: 'privilegeDesc', width: 100, title: '权限描述'}
            , {field: 'status', width: 100, title: '权限状态',templet: function (privilege) {
                    if (privilege.status == '0') {
                        return '正常';
                    } else if (privilege.status == '1') {
                        return '无效';
                    }
                }}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barRole'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'privilegeTable'
    });

    table.on('tool(privilege-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该权限吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"privilegeId": data.privilegeId},
                    url: "http://localhost:8080/privilege/deletePrivilege",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });
            });
        } else if (obj.event === 'edit') {
            editPrivilege = data;
            var index = layer.open({
                type: 2,
                title: "权限修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/privilege-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });


        var $ = layui.$, active = {
        reload: function () {
            var privilegeId = $('#privilegeId');
            var privilegeName = $('#privilegeName');
            //执行重载
            table.reload('privilegeTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    id: privilegeId.val(),
                    name: privilegeName.val()
                }
            }, 'data');
        }
    };

    $('#reloadPrivilege').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


