var editMaterials = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#materials-table'
        , url: 'http://localhost:8080/materials/getAllMaterials'
        , cols: [[
            {field: 'materialsId', title: 'ID',  sort: true}
            , {field: 'materialsCode',  title: '物料编号'}
            , {field: 'materialsName', title: '物料名称'}
            , {field: 'materialsDesc',  title: '物料描述'}
            , {field: 'materialsNum',  title: '物料数量'}
            , {field: 'materialsType',  title: '物料类型',templet: function (materials) {
                    if (materials.materialsType == '0') {
                        return '普通物料';
                    } else if (materials.materialsType == '1') {
                        return '专用物料';
                    }
                }}
            , {field: 'createBy',  title: '创建人'}
            , {field: 'site',  title: '站点'}
            , {field: 'createTime',  title: '创建时间',templet: function (materials) {
                    return layui.util.toDateString(materials.createTime, 'yyyy-MM-dd')
                }}
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'materialsTable'
    });

    table.on('tool(materials-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该用户吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"materialsId": data.materialsId},
                    url: "http://localhost:8080/materials/deleteMaterials",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editMaterials = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "520px"],
                fixed: false, //不固定
                content: "../index/materials-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var materialsId = $('#materialsId');
            var materialsCode = $('#materialsCode');
            //执行重载
            table.reload('materialsTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    materialsId: materialsId.val(),
                    materialsCode: materialsCode.val()
                }
            }, 'data');
        }
    };

    $('#reloadMaterials').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

});


