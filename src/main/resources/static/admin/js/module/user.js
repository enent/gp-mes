var editUser = null;
var roleList = null;
var userId = null;
layui.use('table', function () {
    var table = layui.table
        , form = layui.form;
    table.render({
        elem: '#user-table'
        , url: 'http://localhost:8080/user/getAllUsers'
        , cols: [[
            {field: 'userId', title: 'ID', width: 80, sort: true}
            , {field: 'userNumber', width: 120, title: '用户编号'}
            , {field: 'userName', width: 120, title: '用户名称'}
            , {
                field: 'userStatus', width: 100, title: '用户状态', templet: function (user) {
                    if (user.userStatus == '0') {
                        return '正常';
                    } else if (user.userStatus == '1') {
                        return '无效';
                    }
                }
            }
            , {
                field: 'createTime', width: 200, title: '创建时间', templet: function (user) {
                    return layui.util.toDateString(user.createTime, 'yyyy-MM-dd')
                }
            }
            , {
                field: 'updateTime', width: 200, title: '更新时间', templet: function (user) {
                    return layui.util.toDateString(user.updateTime, 'yyyy-MM-dd')
                }
            }
            , {fixed: 'right', title: '操作', align: 'center', toolbar: '#barUser'}
        ]]
        , page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
            layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
            //,curr: 5 //设定初始在第 5 页
            , limit: 10 //一页显示多少条
            , limits: [5, 10, 15]//每页条数的选择项
            , groups: 3 //只显示 2 个连续页码
            , first: "首页" //不显示首页
            , last: "尾页" //不显示尾页
        }
        , id: 'userTable'
    });

    table.on('tool(user-filter)', function (obj) {
        var data = obj.data;
        if (obj.event === 'del') {
            layer.confirm('确定删除该用户吗', function (index) {
                $.ajax({
                    type: "post",
                    data: {"userId": data.userId},
                    url: "http://localhost:8080/user/deleteUser",
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        alert("系统异常，请联系管理员!");
                    },
                    success: function (data) {
                        layer.alert(data.message, {
                            skin: 'layui-layer-molv' //样式类名  自定义样式
                            , closeBtn: 1    // 是否显示关闭按钮
                            , anim: 1 //动画类型
                            , icon: 6    // icon
                        });
                        obj.del();
                        layer.close(index);
                    }
                });


            });
        } else if (obj.event === 'edit') {
            editUser = data;
            var index = layer.open({
                type: 2,
                title: "用户修改",
                area: ["700px", "420px"],
                fixed: false, //不固定
                content: "../index/user-edit.html",
                cancel: function (index, layero) {
                    // 刷新表格
                    layer.close(index);
                    active.reload();
                    return false;
                }
            });
            return false;
        } else if (obj.event === 'relationRole') {
            form.render();
            userId = data.userId;
            layer.open({
                type: 2
                , title: "分配角色"
                , skin: 'layui-layer-molv'
                , area: ["400px", "320px"]
                , content:"../index/user-role.html"
                ,sucess:function(layero, index){
                }
            })
        }
    });

    var $ = layui.$, active = {
        reload: function () {
            var userNumber = $('#userNumber');
            var userName = $('#userName');
            //执行重载
            table.reload('userTable', {
                page: {
                    curr: 1 //重新从第 1 页开始
                }
                , where: {
                    userNumber: userNumber.val(),
                    userName: userName.val()
                }
            }, 'data');
        }
    };

    $('#reloadUser').on('click', function () {
        var type = $(this).data('type');
        active[type] ? active[type].call(this) : '';
    });

    $.ajax({
        type: "get",
        url: "http://localhost:8080/role/getRoles",
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert("系统异常，请联系管理员!");
        },
        success: function (data) {
            roleList = data.data.roleList;
        }
    })

});

